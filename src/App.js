import './App.css';
import Table from './components/table/table';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <Table/>
    </div>
  );
}

export default App;
