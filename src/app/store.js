import { createStore, combineReducers } from "redux";
import tableEvent from "../components/table/tableEvent"

const appReducer = combineReducers({ 
    tableReducer: tableEvent
});

const store = createStore(
    appReducer
);

export default store;