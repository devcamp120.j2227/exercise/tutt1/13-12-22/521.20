import React from "react";
import { Button, Container, Grid, Input } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";

const Table = () => {
    function createData(stt, noiDung) {
        return { stt, noiDung };
    }

    const dispatch = useDispatch();

    const { tables, tableName }  = useSelector((reduxData) => reduxData.tableReducer);

    const row = tables.map((table) => {
        return createData(table.id, table.name)
    })

    const tableNameHandler = event => {
        dispatch({
            type: "VALUE_HANDLER",
            payload: {
                tableName: event.target.value
            }
        })
    }

    const addTableHandler = event => {
        event.preventDefault();
        let id = 1;
        // Hàm xử lý để lấy id cho table mới 
        if(tables.length) {
            id = Math.max(...tables.map(table => table.id));
            id++;
        }
        console.log("Thêm dòng");
        console.log(row)
        if(tableName) {
            dispatch({
                type: "ADD_TABLE",
                payload: {
                    table: {
                        name: tableName,
                        completed: false,
                        id
                    }
                }
            })
            dispatch({
                type: "VALUE_HANDLER",
                payload: {
                    tableName: ""
                }
            })
        }
    }
    const tableCompletedHandler = event => {
        console.log(event);
        dispatch({
            type: "TOGGLE_COMPLETED_TABLE",
            payload: {
                id: event.target.id
            }
        })
    }

    return (
        <Container>
            <Grid container spacing={2} style={{margin: "0 auto", padding: 20}}>
                <Grid item={true} xs={12}>
                    <form onSubmit={addTableHandler}>
                        <Grid container item={true} xs={12}>
                            <Grid item={true} xs={8}>
                                <Input value={tableName} onChange={tableNameHandler} style={{width: "90%"}}/>
                            </Grid>
                            <Grid item={true} xs={4}>
                                <Button variant="contained" type="submit">Thêm</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>

            <Container>
                <table className="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th align="right">STT</th>
                            <th align="right">Nội dung</th>
                        </tr>
                    </thead>
                    <tbody>
                            {row.map((row) => {
                                return (
                                    <tr key={row.stt}>
                                        <th style={{color: row.completed ? "green" : "red"}} onClick={tableCompletedHandler} id={row.id}>
                                            {row.stt}
                                        </th>
                                        <th style={{color: row.completed ? "green" : "red"}} onClick={tableCompletedHandler} id={row.id}>
                                            {row.noiDung}
                                        </th>    
                                    </tr>
                                )
                            })}
                    </tbody>
                </table>
            </Container>
        </Container>
    )
}

export default Table