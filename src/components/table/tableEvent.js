const initialState = {
    tableName: "",
    tables: []
};

const tableEvent = (state = initialState, action) => {
    switch (action.type) {
        case "VALUE_HANDLER": {
            return {
                ...state,
                tableName: action.payload.tableName
            }
        }
        case "ADD_TABLE": {
            return {
                ...state,
                tables: [...state.tables, action.payload.table]
            }
        }
        case "TOGGLE_COMPLETED_TABLE": {
            const toggleTable = table => {
                if(table.id.toString() === action.payload.id.toString()) {
                    table.completed = !table.completed;
                }
                return table;
            };
            return {
                ...state,
                tables: state.tables.map(toggleTable)
            };
        }
        default: {
            return state;
        }
    }
}

export default tableEvent